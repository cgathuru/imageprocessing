#include "lodepng.h"
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/time.h>
#define THRESHOLD 200

/* TODO: it may help to put some global variables here 
for your threads to use */
struct args_struct {
  int noPixels, startPixel;
};

unsigned char *original_image, *processed_image;
unsigned image_height, image_width;

pthread_mutex_t lock;

void *worker_thread(void *arg) {
  /* TODO: put image processing code here */
  struct args_struct *args = (struct args_struct *) arg;
  int noPixels = args->noPixels;
  int startPixel = args->startPixel;
  int startHeight = (startPixel / image_width);
  int startWidth = startPixel % image_width;

  unsigned char value;
  
  for (int i = startHeight; i < image_height; i++)
   {
      for (int j = 0; j < image_width; j++)
        {
          if(noPixels == args->noPixels)
          {
              j = startWidth;
          }
           if (original_image[4*image_width*i + 4*j] < THRESHOLD)
           {
             value = 0;
           }
           else
           {
             value = 255;
           }
           //pthread_mutex_lock(&lock);
           processed_image[4*image_width*i + 4*j] = value;
           processed_image[4*image_width*i + 4*j + 1] = value;
           processed_image[4*image_width*i + 4*j + 2] = value;
           processed_image[4*image_width*i + 4*j + 3] = 255;
           //pthread_mutex_unlock(&lock);
           noPixels--;
           if (noPixels == 0)
           {
             // finished work. done
             pthread_exit(NULL);
           }
        }
   } 
  pthread_exit(NULL);
}

void binarize(char* input_filename, char* output_filename, int thread_count)
{
  unsigned error;
  unsigned char *image, *new_image;
  unsigned width, height;

  // load image from PNG into C array
  error = lodepng_decode32_file(&image, &width, &height, input_filename);
  if(error) printf("error %u: %s\n", error, lodepng_error_text(error));
  new_image = malloc(width * height * 4 * sizeof(unsigned char));

  struct timeval start, end; // struct used to compute execution time
  gettimeofday(&start, NULL);  // set starting point

  /* TODO: create your thread team here and send each thread an argument 
  telling it which part of "image" to process 

  remember to join all threads!
  */

  image_height = height;
  image_width = width;

  pthread_t *thread_id = malloc(sizeof(pthread_t) * thread_count);

  original_image = image;
  processed_image = new_image;
  int rc;

  if (pthread_mutex_init(&lock, NULL) != 0)
  {
    printf("\n mutex init failed");
    exit(-1);
  }

  struct args_struct **args = malloc(
    sizeof(struct args_struct *) * thread_count);

  // Decide how to split the work here
  /**
  * Calculate the total number of pixels and divde by number of threads
  * Each thread will get acc pixels across the row first before changing
  * to a different pixel height
  **/
  unsigned totalPixels = width * height;
  unsigned ppthread = totalPixels / thread_count;
  unsigned ppthread_rem = totalPixels % thread_count;

  if (thread_count > totalPixels)
  {
    thread_count = totalPixels; // dont create more threads than tasks
  }

  unsigned currentpx = 0;
  for (int i = 0; i < thread_count; i++)
  {
    args[i] = malloc(sizeof(struct args_struct));
    if (ppthread_rem != 0)
    {
      args[i]->noPixels = ppthread + 1;      
      ppthread_rem--;
    }
    else
    {
      args[i]->noPixels = ppthread;
    }
    args[i]->startPixel = currentpx;
    currentpx = currentpx + args[i]->noPixels;
    rc = pthread_create(&thread_id[i], NULL, worker_thread, (void *) args[i]);
    if (rc)
    {
      printf("ERROR: Return code from pthread_create is %d \n", rc);
      exit(-1);
    }
  }

  for (int i = 0; i < thread_count; i++)
  {   
    pthread_join(thread_id[i], NULL);
    free(args[i]);
  }

  gettimeofday(&end, NULL);
  printf("\n\nAlgorithm's computational part duration : %ld\n", \
               ((end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)));


  lodepng_encode32_file(output_filename, new_image, width, height);

  free(args);
  free(image);
  free(new_image);
  pthread_mutex_destroy(&lock);
}

int main(int argc, char *argv[])
{
  char* input_filename = argv[1];
  char* output_filename = argv[2];
  int thread_count = atoi(argv[3]);

  binarize(input_filename, output_filename, thread_count);

  return 0;
}
