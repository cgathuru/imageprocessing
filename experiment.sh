#!/bin/bash

make clean
make

for (( i = 0; i < 100; i++ )); do
	#./binarize_sequential fail.png failb.png	
	#./binarize_pthreads fail.png failb.png 2
	#./binarize_pthreads fail.png failb.png 4
	#./binarize_pthreads fail.png failb.png 8
	#./binarize_pthreads fail.png failb.png 16
	#./binarize_pthreads fail.png failb.png 32
	#./binarize_openmp fail.png failb.png 2
	./binarize_openmp fail.png failb.png 4
	#./binarize_openmp fail.png failb.png 8
	#./binarize_openmp fail.png failb.png 16
	#./binarize_openmp fail.png failb.png 32
	#./sobel_sequential fail.png failb.png	
	#./sobel_pthreads fail.png failb.png 2
	#./sobel_pthreads fail.png failb.png 4
	#./sobel_pthreads fail.png failb.png 8
	#./sobel_pthreads fail.png failb.png 16
	#./sobel_pthreads fail.png failb.png 32
	#./sobel_openmp fail.png failb.png 2
	#./sobel_openmp fail.png failb.png 4
	#./sobel_openmp fail.png failb.png 8
	#./sobel_openmp fail.png failb.png 16
	#./sobel_openmp fail.png failb.png 32
done
